// $(function(){
// 	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
// 	ua = navigator.userAgent,

// 	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

// 	scaleFix = function () {
// 		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
// 			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
// 			document.addEventListener("gesturestart", gestureStart, false);
// 		}
// 	};
	
// 	scaleFix();
// });
// var ua=navigator.userAgent.toLocaleLowerCase(),
//  regV = /ipod|ipad|iphone/gi,
//  result = ua.match(regV),
//  userScale="";
// if(!result){
//  userScale=",user-scalable=0"
// }
// document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором

var sticky 		= $(".sticky"),
	matchheight = $("[data-mh]"),
	flip		= $(".card"),
	popup 		= $("[data-popup]"),
	owl 		= $(".owl-carousel"),
	rangeSlider = $(".slider-range"),
	styler 		= $(".styler"),
	accordion   = $(".accordion"),
	tabs   		= $("#verticalTab"),
	scrollpane 	= $(".scroll-pane"),
	fancyBox 	= $(".fancybox");


	if(matchheight.length){
		include("js/jquery.matchHeight-min.js");
	}
	if(owl.length){
	  	include("js/owl.carousel.js");
	}
	if(flip.length){
	  	include("js/jquery.flip.min.js");
	}
	if(scrollpane.length){
		include("js/jquery.mousewheel.js");
		include("js/mwheelIntent.js");
		include("js/jquery.jscrollpane.min.js");
	}

	if($(fancyBox).length){
	  include("js/jquery.fancybox.js");
	}
	if(tabs.length){
	  include("js/easyResponsiveTabs.js");
	}
	if(popup.length){
		include('js/jquery.arcticmodal.js');
	}
	if(sticky.length){
		include("js/sticky.js");
		include("js/jquery.smoothscroll.js");
	}
	if(rangeSlider.length){
		include('js/jquery-ui.js');
		include('js/jquery.ui.touch-punch.min.js');
	}
	if(styler.length){
		include("js/formstyler.js");
	}
// Плагин скриптом задает высоту блоку и выставляет по самому большому , нужно чтобы на блоке висел класс (data-mh) и атрибут (data-mh) с одним названием для одинаковых блоков, тогда он будет работать, выше находиться его инициализация, еще он хорошо работает при респонзиве. 

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

  $(".menu_link").on("click", function(){
  	$("body").addClass("navTrue");
  })
  $(".menu_sub").on("mouseleave", function(){
		$("body").removeClass("navTrue");
	})


  	/* ------------------------------------------------
	FLIP START
	------------------------------------------------ */

		if(flip.length){
			$(".card").flip({
		        axis: "x",
		        reverse: true,
		        trigger: "click"
		    });
		}

	/* ------------------------------------------------
	FLIP END
	------------------------------------------------ */


  	/* ------------------------------------------------
	FANCYBOX START
	------------------------------------------------ */

		if (fancyBox.length){
			fancyBox.fancybox({

			});
		}

	/* ------------------------------------------------
	FANCYBOX END
	------------------------------------------------ */


	/* ------------------------------------------------
	TABS START
	------------------------------------------------ */

		if (tabs.length){
			// tabs.responsiveTabs({
			// 	startCollapsed: 'accordion'
			// });

			tabs.easyResponsiveTabs({
		        type: 'vertical', //Types: default, vertical, accordion
		        width: 'auto', //auto or any width like 600px
		        fit: true, // 100% fit in a container
		        closed: 'accordion', // Start closed if in accordion view
		        tabidentify: 'hor_1', // The tab groups identifier
		    });
		}

	/* ------------------------------------------------
	TABS END
	------------------------------------------------ */

	/* ------------------------------------------------
	CAROUSEL START
	------------------------------------------------ */

		if(owl.length){
			owl.owlCarousel({
				items : 3,
				navigation: true,
			    slideSpeed : 1000
			});
		}

	/* ------------------------------------------------
	CAROUSEL END
	------------------------------------------------ */


	

	/* ------------------------------------------------
	SCROLL PANE START
	------------------------------------------------ */

	    if(scrollpane.length){

	    	scrollpane.jScrollPane({
	    		// verticalDragMaxHeight : 40
	    	});
	       
	    }

	/* ------------------------------------------------
	SCROLL PANE END
	------------------------------------------------ */


	/* ------------------------------------------------
	ACCORDION START
	------------------------------------------------ */

			if($('.accordion_item_link').length){
				$('.accordion_item_link').on('click', function(){
					$(this)
					.toggleClass('active')
					.next('.sub-menu')
					.slideToggle()
					.parents(".accordion_item")
					.siblings(".accordion_item")
					.find(".accordion_item_link")
					.removeClass("active")
					.next(".sub-menu")
					.slideUp();
				});  
			}
				$('.accordion_item_link').on('click', function(e) {
				    e.preventDefault();
				});


	/* ------------------------------------------------
	ACCORDION END
	------------------------------------------------ */


	



	/* ------------------------------------------------
	POPUP START
	------------------------------------------------ */

		if(popup.length){
			popup.on('click',function(){
			    var modal = $(this).data("popup");
			    $(modal).arcticmodal({
			    		beforeOpen: function(){
			    	}
			    });
			});
		};

	/* ------------------------------------------------
	POPUP END
	------------------------------------------------ */

	/* ------------------------------------------------
	STICKY START
	------------------------------------------------ */

		if (sticky.length){
			$(sticky).sticky({
		        topspacing: 0,
		        styler: 'is-sticky',
		        animduration: 0,
		        unlockwidth: false,
		        screenlimit: false,
		        sticktype: 'alonemenu'
			});
		};

	/* ------------------------------------------------
	STICKY END
	------------------------------------------------ */


	/* ------------------------------------------------
	RANGE-SLIDER START
	------------------------------------------------ */

		if(rangeSlider.length){

			rangeSlider.slider({
				min: 0,
				max: 400,
				value: 150,
                orientation: "horizontal"
			});  
			rangeSlider.slider();  

			var rangeSlider_h = $(".range_h");
			rangeSlider_h.slider({
				min: 0,
				max: 400,
				value: 150,
                slide: function( event, ui ) {
	                $( "#amount1" ).val( ui.value );
	            }	
			});  
			$( "#amount1" ).on( "change", function() {
		      rangeSlider_h.slider( "value", this.value );
		    });
			rangeSlider_h.slider();  


			var rangeSlider1 = $(".range1");
			rangeSlider1.slider({
				min: 0,
				max: 400,
				value: 150,
                slide: function( event, ui ) {
	                $( "#amount1" ).val( ui.value );
	            }	
			});  
			$( "#amount1" ).on( "change", function() {
		      rangeSlider1.slider( "value", this.value );
		    });
			rangeSlider1.slider();  


			var rangeSlider2 = $(".range2");
			rangeSlider2.slider({
				min: 2.2,
				max: 5,
				step: 0.01,
				value: 3,
                slide: function( event, ui ) {
	                $( "#amount2" ).val( ui.value );
	            }	
			});  
			$( "#amount2" ).on( "change", function() {
		      rangeSlider2.slider( "value", this.value );
		    });
			rangeSlider2.slider();  


			var rangeSlider3 = $(".range3");
			rangeSlider3.slider({
				min: 1,
				max: 50,
				value: 15,
                slide: function( event, ui ) {
	                $( "#amount3" ).val( ui.value );
	            }	
			});  
			$( "#amount3" ).on( "change", function() {
		      rangeSlider3.slider( "value", this.value );
		    });
			rangeSlider3.slider();  
	    }


	/* ------------------------------------------------
	RANGE-SLIDER END
	------------------------------------------------ */

	/* ------------------------------------------------
	FORMSTYLER START
	------------------------------------------------ */

		if (styler.length){
			styler.styler({
				
			});
		}

	/* ------------------------------------------------
	FORMSTYLER END
	------------------------------------------------ */



	function required(input){
		if (!input.val()){
			return false;
		}
		else{
			return true;
		}
	}

	function checkDigit(input){
		var digitPattern = /^\d+$/;
		if (!digitPattern.exec(input.val())){
			$(input).closest('.k-form-row').attr('data-validation', 'error');
		}
		else{
			$(input).closest('.k-form-row').attr('data-validation', 'success');
		}
	}

	function checkEmail(input)
	{
	  var pattern=/^([a-zA-Z0-9_-]+\.)*[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)*\.[a-zA-Z]{2,4}$/;
	  if(!pattern.exec($(input).val()))
		{
	    	return false;
		}
		else{
			return true;
		}
	}

	function checkRange(input){
		var current = $(input),
			minVal  = parseInt(current.data('min')),
			maxVal  = parseInt(current.data('max'));

		if (current.val() < minVal || current.val() > maxVal){
			$(input).closest('.k-form-row').attr('data-validation', 'error');
		}
		else{
			$(input).closest('.k-form-row').attr('data-validation', 'success');
		}
	}

	function checkPhone(input) {
		var pattern=/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
		if(!pattern.exec($(input).val()))
		{
	    	return false;
		}
		else{
			return true;
		}
	}

	function checkDate(input) {
		var pattern = /^[0-9]{4}-(0[1-9]|1[012])-(0[1-9]|1[0-9]|2[0-9]|3[01])$/;
		if(!pattern.exec($(input).val()))
		{
	    	$(input).closest('.k-form-row').attr('data-validation', 'error');
		}
		else{
			$(input).closest('.k-form-row').attr('data-validation', 'success');
		}

	}

	
	// Контактная форма


		$('.send_data').on('submit', function(event){
			event.preventDefault();

			var form = $(this),
				errors 	= 1,
				name 	= form.find('[name="name"]'), 
				phone 	= form.find('[name="phone"]'),
				email 	= form.find('[name="email"]'),
				subject = form.find('[name="subject"]');

			if (!required(name)){
				name.addClass('invalid');
				errors = true;
			}
			else{
				name.removeClass('invalid');
				errors = false;
			}

			if (!required(phone) || !checkPhone(phone)){
				phone.addClass('invalid');
				errors = true;
			}
			else{
				phone.removeClass('invalid');
				errors = false;
			}
			if (email.length){
				if (!required(email) || !checkEmail(email)){
					email.addClass('invalid');
					errors = true;
				}
				else{
					email.removeClass('invalid');
					errors = false;
				}	
			}
		 	
			if(!errors){
		        var A = {
		          action : 'send',
		          name : name.val(),
		          phone : phone.val(),
		          email : email.val(),
		          subject : subject.val()
		        }

		        $.post('sendmail.php' , A , function(data){

		            form.find(".invalid").removeClass("invalid");
		            form.find("input").val('');
		            
		            // $("#thanks_box").arcticmodal();
		            
		            // if (isPopup){
		            // 	$.arcticmodal('close');
		            // }

		            // document.location.href="spasibo.html";

		            
		        });
		     }

		})

		

});